//
//  LoginViewController.m
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/8/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "LoginViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "Connection.h"
#import "User.h"
#import "Server.h"
@interface LoginViewController ()
@property (weak, nonatomic) Connection *connectionManager;
@end
@implementation LoginViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.connectionManager = [Connection connectionManager];
    [self initialisation];
}
- (void) initialisation {
    RAC(self.loginButton, enabled) = [RACSignal combineLatest: @[
                                                                 self.jidTextField.rac_textSignal,
                                                                 self.passwordTextField.rac_textSignal,
                                                                 self.nameTextField.rac_textSignal]
                                                       reduce:^id(NSString *jidText,
                                                                  NSString *password,
                                                                  NSString *name){
                                                           return @(jidText.length>5 && password.length>3 && name.length>2);
                                                       }];
// RAC(self.descriptionLabel, text) = [RACSignal ]
}

- (IBAction)testConnectionButtonTapped:(UIButton *)sender {
    User *user = [User userWithJID:self.jidTextField.text name:self.nameTextField.text initPass:self.passwordTextField.text];
    Server *server = [Server serverWithAddress:self.serverAddressTextField.text port:@"5222"];
    if ([_connectionManager isReadyForConnect]) {
//        NSLog(@"Connected");
    }
}
- (IBAction)loginButtonTapped:(UIButton *)sender {
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
