//
//  Connection.m
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/8/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "Connection.h"
#import "User.h"
#import "Server.h"
#import <XMPPFramework/XMPPFramework.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
@interface Connection()
//@property (weak, nonatomic) User *user;

@end
static Connection *shared;
@implementation Connection
@synthesize xmppStream, jid;

+ (id)connectionManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        xmppStream = [[XMPPStream alloc] init];
        [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return self;
}

//- (XMPPStream *)xmppStream {
//    return [self xmppStream];
//}
- (void)xmppStreamDidConnect:(XMPPStream *)sender {
    NSLog(@"%@", sender.isConnected? @"!!!Connected!!!":@"NO");
    isOpen = YES;
    NSError *error = nil;
    User *user = [User sharedInstance];
    [self.xmppStream authenticateWithPassword:user.password error:&error];
    if (error) {
        NSLog(@"%@",error.localizedDescription);
    }
}
- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    [self goOnline];
}

- (void)getConnection {
    TURNSocket *turnSocket = [[TURNSocket alloc] initWithStream:[self xmppStream] toJID:[self jid]];
    [turnSockets addObject:turnSocket];
    [turnSocket startWithDelegate:self delegateQueue:dispatch_get_main_queue()];
}
- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence {
    
}

- (BOOL)isReadyForConnect {
    User *user = [User sharedInstance];
    NSLog(@"%@", user.jidString);
    Server *server = [Server sharedInstace];
    NSLog(@"%@", server.address);
    self.xmppStream.hostName = server.address;
    self.xmppStream.hostPort = [server.port integerValue];
    jid = [XMPPJID jidWithString:user.jidString];
    [self.xmppStream setMyJID:jid];
    NSError *error = nil;
    [self.xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
        return NO;
    } else {
        NSLog(@"Connection...");
        return YES;
    }
    
//    self.xmppStream.myJID = jid;
    
//    [xmppStream connectTo:jid withAddress:server.address withTimeout:10 error:&error];
    return xmppStream.isConnected;
//    [[self xmppStream] setValidatesResponses:YES];
}
//- (RACSignal *)getConnectionStatus {
//    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
//        [subscriber  //[self xmppStream] isConnected
//    }
//}


- (void)goOnline {
    XMPPPresence *presenceStatus = [XMPPPresence presence];
    [[self xmppStream] sendElement:presenceStatus];
}
- (void)goOffline {
    XMPPPresence *presenceStatus = [XMPPPresence presenceWithType:@"unavailable"];
    [[self xmppStream] sendElement:presenceStatus];
}

@end
