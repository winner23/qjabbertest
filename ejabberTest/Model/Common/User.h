//
//  User.h
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/7/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSInteger, Presence) {
    chat,   //Indicates that the contact is available and ready to chat. (Green)
    dnd,    //Indicates “Do Not Disturb” and that the contact is busy for chat. (Red)
    away,   //Indicates that the contact is idle and possibly not at seat. (Amber)
    xa      //Indicates “Extended Away” (Dark amber)
};
@interface User : NSObject

@property (strong, nonatomic) NSString * _Nonnull jidString;
@property (strong, nonatomic) NSString * _Nonnull name;
@property (strong, nonatomic) NSString * _Nonnull password;
@property (weak, nonatomic) NSArray * _Nullable groups;
@property (assign, nonatomic) Presence presence;
+ (instancetype _Nonnull )sharedInstance;
+ (instancetype _Nonnull )userWithJID:(NSString *_Nonnull)jid name:(NSString *_Nonnull)name initPass:(NSString *_Nonnull)pass;
- (NSDictionary *_Nonnull)dictionary;
- (BOOL) checkPass: (NSString *_Nonnull)word;

@end
