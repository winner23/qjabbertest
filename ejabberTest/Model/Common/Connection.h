//
//  Connection.h
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/8/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
@class XMPPStream;
@class XMPPJID;
@interface Connection : NSObject {
    NSMutableArray *turnSockets;
    BOOL isOpen;
}
@property(strong, nonatomic) XMPPStream *xmppStream;
@property(strong, nonatomic) XMPPJID *jid;

+ (id)connectionManager;

- (BOOL)isReadyForConnect;
- (void)getConnection;

- (void)goOnline;
- (void)goOffline;
@end
