//
//  Server.h
//  ejabberTest
//
//  Created by Volodymyr Vinyarskyy on 11.02.2018.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Server : NSObject
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *port;
+ (instancetype)sharedInstace;
+ (instancetype)serverWithAddress:(NSString *)address port:(NSString *)port;
@end
