//
//  MessageDelegate.h
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/13/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MessageDelegate <NSObject>
- (void)newMessageReceived;
@end
