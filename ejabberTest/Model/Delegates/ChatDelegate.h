//
//  ChatDelegate.h
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/13/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChatDelegate <NSObject>

- (void)newUserOnline:(NSString *)userName;
- (void)userWhenOffline:(NSString *)userNmae;
- (void)didDisconnect;

@end
