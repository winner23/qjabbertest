//
//  main.m
//  ejabberTest
//
//  Created by Volodymyr Viniarskyi on 2/7/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
